# brave

Web browser that blocks ads and trackers by default (binary release)

https://github.com/brave/brave-browser

https://gitlab.manjaro.org/packages/community/brave

<br>
HOw to clone this repository:
```
git clone https://gitlab.com/azul4/content/web-browsers/brave.git
```
